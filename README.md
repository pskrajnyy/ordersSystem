# Project info
A console application implemented in Java. It is a simple system of handling orders (eg online store). Orders can be added in the console or from a JSON file. You can also make a copy of all orders to a JSON file.

# Project insight
![App](/misc/1.png)
![App](/misc/2.png)
![App](/misc/3.png)