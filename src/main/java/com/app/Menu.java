package com.app;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.Month;
import java.util.*;

public class Menu {
    static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        LinkedHashSet<Orders> orders = new LinkedHashSet<>();

        addExamplesOrders(orders);

        int number = orders.size();

        while(true) {
            printMenu();
            int choose = scan.nextInt();
            chooseOptInMenu(orders, number, choose);
        }
    }

    private static void chooseOptInMenu(LinkedHashSet<Orders> orders, int number, int choose) throws IOException {
        switch (choose) {
            case 1:
                System.out.println("Liczba zamówień wynosi: " + orders.size());
                break;
            case 2:
                showOrders(orders);
                break;
            case 3:
                orders.add(new Orders().addOrder(number));
                break;
            case 4:
                removeOrders(orders);
                break;
            case 5:
                totalPrice(orders);
                break;
            case 6:
                saveToJson(orders);
                break;
            case 7:
                List<Orders> result = readFromJson();
                orders.addAll(result);
                break;
            case 9:
                System.out.println("Dziekuje za skorzystanie z mojego programu");
                System.exit(0);
            default:
                System.out.println("Podałeś niepoprawny numer");
                break;
        }
    }

    private static void printMenu() {
        System.out.println(" " + "\n ");
        System.out.println("Co byś chciał zrobić?");
        System.out.println("1.Wyświetlić ilość zamówień" +
                "\n2.Wyświetlić listę zamówień" +
                "\n3.Dodać zamówienie" +
                "\n4.Usunąc zamówienie" +
                "\n5.Łączny koszt zamówionych towarów" +
                "\n6.Zrobić kopię do pliku" +
                "\n7.Dodać zamówienia z pliku" +
                "\n9.Wyjść" +
                "\nWpisz numerek: ");
    }

    private static void addExamplesOrders(LinkedHashSet<Orders> orders) {
        Orders order1 = new Orders("Przemek", "Skrajny", "telefon",669.99, 2018, Month.JANUARY, 15);
        Orders order2 = new Orders("Patrycja", "Łomińska", "telewizor",1999.99,2018, Month.JANUARY, 11);
        Orders order3 = new Orders("Klaudia", "Skrajna", "łóżko",15.20,2018, Month.JANUARY, 10);
        Orders order4 = new Orders("Klaudia", "Skrajna","latarka",15.20,2018, Month.JANUARY, 13);

        orders.add(order1);
        orders.add(order2);
        orders.add(order3);
        orders.add(order4);
    }

    public static void removeOrders (LinkedHashSet<Orders> orders) {
        System.out.println("Podaj numer zamówienia które chcesz usunać: ");
        int numberRemove = scan.nextInt();
        int a=0;
        Orders toRemove = null;
        for(Orders value: orders) {
            if(a==numberRemove) {
                toRemove = value;
            }
            a++;
        }
        orders.remove(toRemove);
        if(numberRemove > orders.size()) {
            System.out.println("Nie ma takiego zamówienia");
        }
    }

    public static void showOrders(LinkedHashSet<Orders> orders) {
        System.out.println("Lista zamówień: ");
        int number = 0;
        for(Orders value: orders) {
            System.out.println(number + ". " + value);
            number++;
        }
    }

    public static void totalPrice(LinkedHashSet<Orders> orders) {
        double totalPrice = 0;
        for(Orders value: orders) {
            totalPrice = totalPrice + value.getPrice();
        }
        System.out.println("Łączny koszt zamówionych towarów wynosi: " + totalPrice + " zł");
    }

    public static void saveToJson(LinkedHashSet<Orders> orders) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String date = LocalDate.now().toString();
        mapper.writerWithDefaultPrettyPrinter().writeValue(new File(date + ".json"), orders);
    }

    public static List<Orders> readFromJson() throws IOException {
        System.out.println("Podaj nazwę pliku z którego chcesz wczytać zamówienia: ");
        String fileName = scan.nextLine();
        ObjectMapper mapper = new ObjectMapper();
        List<Orders> order = mapper.readValue(new File(fileName + ".json"),new TypeReference<List<Orders>>(){});
        return order;
    }
}