package com.app;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import java.time.LocalDate;
import java.time.Month;
import java.util.Scanner;

class Orders
{
    private int number;
    private String name;
    private String surname;
    private String product;
    private double price;
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate date;

    Scanner scan = new Scanner(System.in);

    public Orders(String name, String surname, String product, double price, int orderYear, Month orderMonth, int orderDay) {
        this.name = name;
        this.surname = surname;
        this.product = product;
        this.price = price;
        this.date = LocalDate.of(orderYear, orderMonth, orderDay);
    }

    public Orders() {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Orders orders = (Orders) o;

        if (Double.compare(orders.price, price) != 0) return false;
        if (!name.equals(orders.name)) return false;
        if (!surname.equals(orders.surname)) return false;
        if (!product.equals(orders.product)) return false;
        return date.equals(orders.date);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = name.hashCode();
        result = 31 * result + surname.hashCode();
        result = 31 * result + product.hashCode();
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + date.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "{" + "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", product='" + product + '\'' +
                ", price=" + price + " zł" +
                ", date=" + date +
                '}';
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getProduct() {
        return product;
    }

    public double getPrice() {
        return price;
    }

    public LocalDate getDate() {
        return date;
    }

    public Orders addOrder(int number) {
        System.out.println("Dodawanie zamówienia");
        System.out.println("Podaj imie zamawiającego: ");
        name = scan.nextLine();
        System.out.println("Podaj nazwisko zamawiającego: ");
        surname = scan.nextLine();
        System.out.println("Zamówiony produkt: ");
        product = scan.nextLine();
        System.out.println("Podaj cene: ");
        price = scan.nextDouble();
        int orderYear = LocalDate.now().getYear();
        Month orderMonth = LocalDate.now().getMonth().firstMonthOfQuarter();
        int orderDay = LocalDate.now().getDayOfMonth();
        number++;
        Orders order = new Orders(name, surname, product, price, orderYear,orderMonth,orderDay);
        return order;
    }
}